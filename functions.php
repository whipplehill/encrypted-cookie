<?php

//Key from Podium settings screen: Platform Manager - Security - Encrypted Cookie
$podium_key = ")B0UCTaP/PE8Z:M_,;@0SM8LD27^+7Qa:V^CH\?WAa0V\P33/B";	

//Grab cookie set by podium
//Value should be loaded from cookie called "integration" 
//Name can be whatever you set on settings screen
$cookie_value = $_COOKIE["integration"];//

//output variables to screen for debugging reference
echo 'podium_key: '.$podium_key.'<br />';
echo 'cookie_value: '.$cookie_value.'<br />';
echo"<br>";

// decrypt cookie into array
$result = process_cookie($cookie_value, $podium_key);
print_r($result);

//decrypt single part
//echo decrypt_cookie($result['l'], $podium_key );
function process_cookie( $cookie_value , $podium_key = false) {
	
	
	$newCookie = array();
	//break cookie into three parts ie for username,firstname,lastname
	$cval = explode("&f=", $cookie_value);
	$newCookie[0] = $cval[0];
	
	$cval1 = explode("&l=", $cval[1]);
	$newCookie[1] = 'f='.$cval1[0];
	//$newCookie[2] = 'l='.$cval1[1];

	$cval2 = explode("&h=", $cval1[1]);
	$newCookie[2] = 'l='.$cval2[0];
	$newCookie[3] = 'h='.$cval2[1];

	$args = array( );
	
	foreach($newCookie as $chunk)
	{
		//it will explode on the first '='  no matter ho many equal comes in the value
		$chunk = explode('=', $chunk, 2);
		list($key, $val) = $chunk;

		if($podium_key == false){
			$args[ $key ] =  urldecode( $val );
		} else {
			$args[ $key ] =  decrypt_cookie(  $val  , $podium_key);
		}
	}
	return $args;
}



/**
 * decrypt_cookie - support function for process cookie
 * 
 * @param mixed $cookie_part the single piece of cookie to decrypt.
 * @param mixed $podium_key  key from podium.
 *
 * @access public
 *
 * @return string decrypted cookie value.
 */

function decrypt_cookie($cookie_part, $podium_key)
{
	$outputstr = "";
	for( $i = 0; $i < strlen($cookie_part); $i++ )
	{
		$x = substr($cookie_part, $i, 1);
		$t = substr($podium_key, $i, 1); // $podium_key[$i];
		//echo $x.''.$t . '<br>';
		$outputstr .= $x ^ $t;
	}
	return $outputstr;
}
